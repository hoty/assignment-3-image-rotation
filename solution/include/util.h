#include "image.h"
#include <stdio.h>
#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

enum file_status  {
	FILE_OK = 0,
	FILE_ERROR
};

enum file_status open_file(FILE **f, const char *fname);
enum file_status create_file(FILE **f, const char *fname);
enum file_status close_file(FILE *f);

struct image rotate( struct image const source );

char* read_status_to_string(enum read_status status);
char* write_status_to_string(enum write_status status);
char* file_status_to_string(enum file_status status);

#endif // UTIL_H_INCLUDED
