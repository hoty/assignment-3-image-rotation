#include "image.h"
#include <stdio.h>
#ifndef BMP_H_INCLUDED
#define BMP_H_INCLUDED


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_NOT_SUPPORTED,
  READ_CANT_READ,
  READ_MEMORY_ALLOC
};

enum read_status from_bmp( FILE* in, struct image* img );
//enum read_status load_bmp( const char *fname, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_MEMORY_ALLOC
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif // BMP_H_INCLUDED
