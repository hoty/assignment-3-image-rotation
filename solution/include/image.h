#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include <inttypes.h>

struct pixel {
	union {
    struct {unsigned char b,g,r,a;};
    uint32_t bits;
  };
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void init_image(struct image * img);
void set_image(struct image * img, uint64_t width, uint64_t height);
void free_image(struct image * img);

#endif // IMAGE_H_INCLUDED
