#include "../include/bmp.h"
#include "../include/util.h"
#include <stdio.h>

int load_bmp( const char *fname, struct image* img )
{
	FILE *f;
	int rez = open_file( &f, fname );
	if( rez !=0 ) { fprintf(stderr,"%s\n",file_status_to_string(rez)); return rez;}

	rez = from_bmp(f,img);
	if( rez !=0 ) { fprintf(stderr,"%s\n",read_status_to_string(rez)); close_file(f); return rez; }

	rez = close_file(f);
	if( rez !=0 ) { fprintf(stderr,"%s\n",file_status_to_string(rez)); }
	return rez;
}

int save_bmp( const char *fname, struct image* img )
{
	FILE *f;
	int rez = create_file( &f, fname );
	if( rez !=0 ) { fprintf(stderr,"%s\n",file_status_to_string(rez)); return rez;}

	rez = to_bmp(f,img);
	if( rez !=0 ) { fprintf(stderr,"%s\n",write_status_to_string(rez)); close_file(f); return rez; }

	rez = close_file(f);
	if( rez !=0 ) { fprintf(stderr,"%s\n",file_status_to_string(rez)); }
	return rez;
}

int main(int argc, char* argv[])
{
	if (argc == 3) {
		struct image img;
		init_image(&img);

		if (load_bmp(argv[1],&img)!=0) {
			fprintf(stderr,"Load source image error\n");
			return 0;
		}

		struct image rez = rotate(img);

		if (save_bmp(argv[2],&rez)!=0) {
			fprintf(stderr,"Save transformed image error\n");
			return 0;
		}

		free_image(&img);
		free_image(&rez);
	} else {
		fprintf(stderr,"Must have arguments <source-image> <transformed-image>\n");
	}
  return 0;
}
