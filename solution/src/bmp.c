#include "../include/bmp.h"
#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include <string.h>

#define DISC_BUFF 1024
#define SIGNATURE_BM 0x4d42
#define SIGNATURE_IC 0x4349
#define SIGNATURE_PT 0x5450

#define FILE_HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define HEADER_SIZE ((FILE_HEADER_SIZE)+(INFO_HEADER_SIZE))

#define COLOR_24_BYTES 3
#define COLOR_24_RES 24
#define LINE_RESERVED_BYTES 3

#define BMP_FILE_HEAD_SIZE 2
#define BMP_FILE_HEAD_DATA 10
#define BMP_UNFO_HEAD_WIDTH 4
#define BMP_UNFO_HEAD_HEIGHT 8
#define BMP_UNFO_HEAD_PLANES 12
#define BMP_UNFO_HEAD_BIT 14
#define BMP_UNFO_HEAD_COMPRESS 16

uint64_t max_width_bytes(uint32_t bpp, uint64_t width) { return (bpp*width+LINE_RESERVED_BYTES) & (-4); }
uint64_t width_bytes(uint32_t bpp, uint64_t width) { return bpp*width; }

enum read_status from_bmp( FILE* in, struct image* img )
{
  fseek( in, 0, SEEK_END);
  uint32_t filesize = ftell(in);
  fseek( in, 0, SEEK_SET);

  size_t res;
  uint8_t bmpfileheader[FILE_HEADER_SIZE];
  uint8_t bmpinfoheader[INFO_HEADER_SIZE];

  res = fread( bmpfileheader, 1, FILE_HEADER_SIZE, in);
  if( (int)res != FILE_HEADER_SIZE ) return READ_CANT_READ;
  res = fread( bmpinfoheader, 1, INFO_HEADER_SIZE, in);
  if( (int)res != INFO_HEADER_SIZE ) return READ_CANT_READ;

  int bfType = *(uint16_t*)(bmpfileheader);
  if( bfType!=SIGNATURE_BM && bfType!=SIGNATURE_IC && bfType!=SIGNATURE_PT ) return READ_INVALID_SIGNATURE;

    uint32_t bfSize = *(uint32_t*)(bmpfileheader+BMP_FILE_HEAD_SIZE);
    uint32_t bfOffBits = *(uint32_t*)(bmpfileheader+BMP_FILE_HEAD_DATA);
  //int biSize = *(uint32_t*)(bmpinfoheader);
	if (bfSize != filesize) return READ_INVALID_HEADER;

    uint32_t biWidth = *(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_WIDTH);
    uint32_t biHeight = *(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_HEIGHT);

	int biPlanes = *(uint16_t*)(bmpinfoheader+BMP_UNFO_HEAD_PLANES);
	int biBitCount = *(uint16_t*)(bmpinfoheader+BMP_UNFO_HEAD_BIT);
    uint32_t biCompression = *(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_COMPRESS);

	int bits = 0;
	if (biBitCount==COLOR_24_RES) bits=COLOR_24_BYTES;
	if (bits == 0) return READ_INVALID_BITS;

	if (biCompression != 0 || biPlanes != 1) return READ_NOT_SUPPORTED;

	set_image(img,biWidth,biHeight);
	if (img->data==NULL) return READ_MEMORY_ALLOC;

    uint32_t wbytes = max_width_bytes(bits,biWidth);
	filesize = wbytes*biHeight;
	unsigned char *tmp_buf = malloc(wbytes*biHeight);
	unsigned char *p = tmp_buf;

	fseek( in, (long) bfOffBits, SEEK_SET);
	while (filesize>DISC_BUFF) {
		res = fread( p, 1, DISC_BUFF, in );
		if( (int)res != DISC_BUFF ) { free(tmp_buf); return READ_CANT_READ; }
		filesize-=DISC_BUFF;
		p+=DISC_BUFF;
	}
	res = fread( p, 1, filesize, in );
	if( (int)res != filesize ) { free(tmp_buf); return READ_CANT_READ; }

	struct pixel *ptr = img->data;
	for (int y = ((int)biHeight)-1; y >= 0; y--)
	{
    unsigned char *pRow = tmp_buf + wbytes*y;
    for(int x=0; x< biWidth; x++) {
			ptr->bits=pRow[0]+(pRow[1]<<8)+(pRow[2]<<16);
			ptr++;
			pRow+=bits;
    }
	}
	free(tmp_buf);

	return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img )
{
	size_t res;
    uint8_t bmpfileheader[FILE_HEADER_SIZE];
    for(int i=0; i< FILE_HEADER_SIZE; i++) bmpfileheader[i]=0;
    uint8_t bmpinfoheader[INFO_HEADER_SIZE];
    for(int i=0; i< INFO_HEADER_SIZE; i++) bmpinfoheader[i]=0;

    uint64_t biWidth = img->width;
    uint64_t biHeight = img->height;

    uint64_t wbytes = max_width_bytes(COLOR_24_BYTES,biWidth);
    uint64_t reserved = wbytes-width_bytes(COLOR_24_BYTES,biWidth);
    uint64_t filesize = HEADER_SIZE + wbytes*biHeight;

	*(uint16_t*)(bmpfileheader) = SIGNATURE_BM;
	*(uint32_t*)(bmpfileheader+BMP_FILE_HEAD_SIZE) = filesize;
	*(uint32_t*)(bmpfileheader+BMP_FILE_HEAD_DATA) = HEADER_SIZE;
	*(uint32_t*)(bmpinfoheader) = INFO_HEADER_SIZE;
	*(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_WIDTH) = biWidth;
	*(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_HEIGHT) = biHeight;
	*(uint16_t*)(bmpinfoheader+BMP_UNFO_HEAD_PLANES) = 1;
	*(uint16_t*)(bmpinfoheader+BMP_UNFO_HEAD_BIT) = COLOR_24_RES;
	*(uint32_t*)(bmpinfoheader+BMP_UNFO_HEAD_COMPRESS) = 0;

  res = fwrite( bmpfileheader, 1, FILE_HEADER_SIZE, out);
  if( (int)res != FILE_HEADER_SIZE ) return WRITE_ERROR;
  res = fwrite( bmpinfoheader, 1, INFO_HEADER_SIZE, out);
  if( (int)res != INFO_HEADER_SIZE ) return WRITE_ERROR;

	struct pixel *ptr = img->data;
	if (ptr==NULL) return WRITE_MEMORY_ALLOC;
  unsigned char *tmp_buf = malloc(wbytes*biHeight);
  unsigned char *p = tmp_buf;

	for (long long y = ((long long) biHeight)-1; y >= 0; y--)
	{
    unsigned char *pRow = tmp_buf + wbytes*y;
    for(int x=0; x< biWidth; x++) {
			*pRow = ptr->b; pRow++;
			*pRow = ptr->g; pRow++;
			*pRow = ptr->r; pRow++;
			ptr++;
    }
    for(int x=0; x< reserved; x++)
			pRow[x]=0;
	}
	filesize -= HEADER_SIZE;

	while (filesize>DISC_BUFF) {
		res = fwrite( p, 1, DISC_BUFF, out );
		if( (int)res != DISC_BUFF ) { free(tmp_buf); return WRITE_ERROR; }
		filesize-=DISC_BUFF;
		p+=DISC_BUFF;
	}
	res = fwrite( p, 1, filesize, out );
	if( (int)res != filesize ) { free(tmp_buf); return WRITE_ERROR; }

  free(tmp_buf);

	return WRITE_OK;
}
