#include "../include/bmp.h"
#include "../include/util.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum file_status open_file(FILE **f, const char *fname)
{
	*f = fopen( fname, "rb" );
	if( !*f ) return FILE_ERROR;
	return FILE_OK;
}

enum file_status create_file(FILE **f, const char *fname)
{
	*f = fopen( fname, "wb" );
	if( !*f ) return FILE_ERROR;
	return FILE_OK;
}

enum file_status close_file(FILE *f)
{
	if (fclose(f)!=0) return FILE_ERROR;
	return FILE_OK;
}

void rotate_image_forward_90(struct image* img)
{
	struct pixel* newdata = malloc(img->width*img->height*sizeof(struct pixel));

    uint64_t w =img->width;
    uint64_t h =img->height;
	struct pixel *rRow = img->data;
	for(int y = (int) h-1; y >= 0; y--) {
		struct pixel *wRow = newdata + y;
		for(uint64_t x=0; x< w; x++) {
			*wRow = *rRow;
			rRow++;
		  wRow+=h;
		}
	}

	img->height = w;
	img->width = h;
	free(img->data);
	img->data = newdata;
}

void rotate_image_back_90(struct image* img)
{
	struct pixel* newdata = malloc(img->width*img->height*sizeof(struct pixel));

    uint64_t w =img->width;
    uint64_t h =img->height;
	struct pixel *rRow = img->data;
    uint64_t base = h*(w-1);
	for(int y = 0; y < h; y++) {
		struct pixel *wRow = newdata + base + y;
		for(int x=0; x< w; x++) {
			*wRow = *rRow;
			rRow++;
		  wRow-=h;
		}
	}

	img->height = w;
	img->width = h;
	free(img->data);
	img->data = newdata;
}

struct image rotate( struct image const source )
{
	struct image rez;
	init_image(&rez);
	set_image(&rez,source.height,source.width);
	if (rez.data==NULL) return rez;

    uint64_t w =source.width;
    uint64_t h =source.height;
	struct pixel *rRow = source.data;
    uint64_t base = h*(w-1);
	for(int y = 0; y < h; y++) {
		struct pixel *wRow = rez.data + base + y;
		for(int x=0; x< w; x++) {
			*wRow = *rRow;
			rRow++;
		  wRow-=h;
		}
	}
	return rez;
}

char* read_status_to_string(enum read_status status)
{
	switch (status) {
		case READ_OK: return "ok";
		case READ_INVALID_SIGNATURE: return "invalid signature";
		case READ_INVALID_BITS: return "invalid bits";
		case READ_INVALID_HEADER: return "invalid header";
		case READ_CANT_READ: return "can`t read";
		case READ_NOT_SUPPORTED: return "not supported";
		case READ_MEMORY_ALLOC: return "memory allocation error";
		default: return strerror( errno );
	}
	return "unknown";
}

char* write_status_to_string(enum write_status status)
{
	switch (status) {
		case WRITE_OK: return "ok";
		case WRITE_ERROR: return "error";
		case WRITE_MEMORY_ALLOC: return "memory allocation error";
	}
	return "unknown";
}

char* file_status_to_string(enum file_status status)
{
	switch (status) {
		case FILE_OK: return "ok";
		case FILE_ERROR: return strerror( errno );
	}
	return "unknown";
}
