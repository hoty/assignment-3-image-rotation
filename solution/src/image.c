#include "../include/image.h"
#include <malloc.h>

void init_image(struct image * img)
{
	img->width = 0;
	img->height = 0;
	img->data = NULL;
}

void set_image(struct image * img, uint64_t width, uint64_t height)
{
	if (img->data!=NULL)
		free(img->data);
	img->width = width;
	img->height = height;
	img->data = malloc(width*height*sizeof(struct pixel));
}

void free_image(struct image * img)
{
	if (img->data!=NULL)
		free(img->data);
	img->width = 0;
	img->height = 0;
	img->data = NULL;
}
